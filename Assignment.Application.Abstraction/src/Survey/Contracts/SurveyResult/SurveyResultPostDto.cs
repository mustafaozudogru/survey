﻿using System;

namespace Assignment.Application.Abstraction.src.Survey.Contracts
{
    public class SurveyResultPostDto
    {
        /// <summary>
        /// IpAddress.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// ResultJson.
        /// </summary>
        public string ResultJson { get; set; }
    }
}
