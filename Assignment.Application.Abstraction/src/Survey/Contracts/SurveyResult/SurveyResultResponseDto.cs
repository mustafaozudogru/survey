﻿using System;

namespace Assignment.Application.Abstraction.src.Survey.Contracts
{
    /// <summary>
    /// Used to keep survey update model props.
    /// </summary>
    public class SurveyResultResponseDto
    {
        /// <summary>
        /// IpAddress.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// SurveyJson.
        /// </summary>
        public string ResultJson { get; set; }
    }
}
