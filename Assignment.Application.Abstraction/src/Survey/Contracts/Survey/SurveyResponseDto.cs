﻿using System;

namespace Assignment.Application.Abstraction.src.Survey.Contracts
{
    /// <summary>
    /// Used to keep survey update model props.
    /// </summary>
    public class SurveyResponseDto
    {
        /// <summary>
        /// Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// SurveyJson.
        /// </summary>
        public string SurveyJson { get; set; }
    }
}
