﻿using System;

namespace Assignment.Application.Abstraction.src.Survey.Contracts
{
    /// <summary>
    /// Used to keep survey create model props.
    /// </summary>
    public class SurveyUpdateDto
    {
        /// <summary>
        /// Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// SurveyJson.
        /// </summary>
        public string SurveyJson { get; set; }
    }
}
