﻿using Assignment.Application.Abstraction.src.Survey.Contracts;
using Assignment.Common.src.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assignment.Application.Abstraction.src.Survey
{
    /// <summary>
    /// Survey service interface.
    /// </summary>
    public interface ISurveyService
    {
        /// <summary>
        /// Used to get survey list.
        /// </summary>
        /// <returns>Returns found survey list.</returns>
        Task<ServiceResponse<List<SurveyResponseDto>>> GetAsync();

        /// <summary>
        /// Used to get survey by id list.
        /// </summary>
        /// <param name="id">Survey id.</param>
        /// <returns>Returns found survey list.</returns>
        Task<ServiceResponse<SurveyResponseDto>> GetByIdAsync(Guid id);

        /// <summary>
        /// Used to get survey by name list.
        /// </summary>
        /// <param name="name">Survey name.</param>
        /// <param name="ipAddress">Ip address.</param>
        /// <returns>Returns found survey list.</returns>
        Task<ServiceResponse<List<SurveyResponseDto>>> GetByNameByWithIpControlAsync(string name, string ipAddress);

        /// <summary>
        /// Used to create survey.
        /// </summary>
        /// <param name="request">SurveyCreateDto.</param>
        /// <returns>Returns result.</returns>
        Task<ServiceResponse<SurveyCreateDto>> CreateAsync(SurveyCreateDto request);

        /// <summary>
        /// Used to update survey.
        /// </summary>
        /// <param name="request">SurveyUpdateDto.</param>
        /// <returns>Returns update result.</returns>
        Task<ServiceResponse<SurveyResponseDto>> UpdateAsync(SurveyUpdateDto request);

        /// <summary>
        /// Used to delete survey.
        /// </summary>
        /// <param name="id">Survey id.</param>
        /// <returns>Returns delete result.</returns>
        Task<ServiceResponse<bool>> DeleteAsync(Guid id);

        /// <summary>
        /// Used to create survey result survey.
        /// </summary>
        /// <param name="request">SurveyResultPostDto.</param>
        /// <returns>Returns survey-result create result.</returns>
        Task<ServiceResponse<bool>> SurveyResultPostAsync(SurveyResultPostDto request);

        /// <summary>
        /// Used to get survey results survey.
        /// </summary>
        /// <returns>Returns survey-results.</returns>
        Task<ServiceResponse<List<SurveyResultResponseDto>>> GetSurveyResultsAsync();
    }
}
