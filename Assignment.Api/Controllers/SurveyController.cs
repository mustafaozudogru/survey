﻿using Assignment.Application.Abstraction.src.Survey;
using Assignment.Application.Abstraction.src.Survey.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Assignment.Common.src.Models;
using AutoMapper;
using Assignment.Api.Models;
using Microsoft.AspNetCore.Http;

namespace Assignment.Api.Controllers
{
    [Route("v1/survey")]
    [ApiController]
    public class SurveyController : Controller
    {
        private readonly IMapper mapper;
        private readonly ISurveyService surveyService;

        public SurveyController(IMapper mapper, ISurveyService surveyService)
        {
            this.mapper = mapper;
            this.surveyService = surveyService;
        }

        /// <summary>
        /// Action to get survey list from the database.
        /// </summary>
        /// <returns>Returns survey list</returns>
        /// <response code="200">Returns a list of all survey from the database</response>
        /// <response code="400">Returned if the sammple couldn't be loaded</response>
        [SwaggerResponse(200, type: typeof(ServiceResponse<List<SurveyResponseModel>>), description: "Success.")]
        [SwaggerResponse(400, type: typeof(ServiceResponse<List<SurveyResponseModel>>), description: "Error")]
        [SwaggerResponse(404, type: typeof(ServiceResponse<List<SurveyResponseModel>>), description: "Not found")]
        [HttpGet("")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await this.surveyService.GetAsync().ConfigureAwait(false);
                var mappingResult = this.mapper.Map<List<SurveyResponseDto>, List<SurveyResponseModel>>(result.Data);

                return this.Ok(mappingResult);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Action to get survey by id from the database.
        /// </summary>
        /// <param name="id">Survey id</param>
        /// <returns>Returns survey</returns>
        /// <response code="200">Returns a survey from the database</response>
        /// <response code="400">Returned if the survey couldn't be loaded</response>
        [SwaggerResponse(200, type: typeof(ServiceResponse<SurveyResponseModel>), description: "Success.")]
        [SwaggerResponse(400, type: typeof(ServiceResponse<SurveyResponseModel>), description: "Error")]
        [SwaggerResponse(404, type: typeof(ServiceResponse<SurveyResponseModel>), description: "Not found")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            try
            {
                var result = await this.surveyService.GetByIdAsync(id).ConfigureAwait(false);

                if (!result.IsSuccessful)
                {
                    return this.NotFound(result);
                }

                var mappingResult = this.mapper.Map<SurveyResponseDto, SurveyResponseModel>(result.Data);

                return this.Ok(mappingResult);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Action to get survey by name from the database.
        /// </summary>
        /// <param name="name">Survey name</param>
        /// <returns>Returns survey</returns>
        /// <response code="200">Returns a survey from the database</response>
        /// <response code="400">Returned if the survey couldn't be loaded</response>
        [SwaggerResponse(200, type: typeof(ServiceResponse<List<SurveyResponseModel>>), description: "Success.")]
        [SwaggerResponse(400, type: typeof(ServiceResponse<List<SurveyResponseModel>>), description: "Error")]
        [SwaggerResponse(404, type: typeof(ServiceResponse<List<SurveyResponseModel>>), description: "Not found")]
        [HttpGet("by-name-with-ip-control")]
        public async Task<IActionResult> GetByNameByWithIpControlAsync(string name)
        {
            try
            {
                string ipAddress = Request?.HttpContext?.Connection?.RemoteIpAddress?.ToString();

                if (string.IsNullOrEmpty(ipAddress))
                {
                    return this.BadRequest(new ServiceResponse<List<SurveyResponseModel>>() { Data = null, IsSuccessful = false, Message = "Ip address couldn't retrieved", MessageCode = StatusCodes.Status500InternalServerError });
                }

                var result = await this.surveyService.GetByNameByWithIpControlAsync(name, ipAddress).ConfigureAwait(false);

                if (!result.IsSuccessful && result.MessageCode == StatusCodes.Status500InternalServerError)
                {
                    return this.BadRequest(result);
                }

                if (!result.IsSuccessful && result.MessageCode == StatusCodes.Status404NotFound)
                {
                    return this.NotFound(result);
                }

                var mappingResult = this.mapper.Map<List<SurveyResponseDto>, List<SurveyResponseModel>>(result.Data);

                return this.Ok(mappingResult);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Action to survey a new survey in the database.
        /// </summary>
        /// <param name="surveyModel">Model to create a new survey</param>
        /// <returns>Returns the created survey</returns>
        /// /// <response code="200">Returned if the survey was created</response>
        /// /// <response code="400">Returned if the model couldn't be parsed or the survey couldn't be saved</response>
        [SwaggerResponse(200, type: typeof(ServiceResponse<SurveyCreateModel>), description: "Success.")]
        [SwaggerResponse(400, type: typeof(ServiceResponse<SurveyCreateModel>), description: "Error")]
        [SwaggerResponse(404, type: typeof(ServiceResponse<SurveyCreateModel>), description: "Not found")]
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] SurveyCreateModel surveyModel)
        {
            try
            {
                var mappingRequest = this.mapper.Map<SurveyCreateModel, SurveyCreateDto>(surveyModel);
                var result = await this.surveyService.CreateAsync(mappingRequest).ConfigureAwait(false);
                var mappingResult = this.mapper.Map<SurveyCreateDto, SurveyCreateModel>(result.Data);

                return this.Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Action to update a survey.
        /// </summary>
        /// <param name="surveyModel">Model to update survey.</param>
        /// <returns>Returns the created survey</returns>
        /// /// <response code="200">Returned if the survey was updated</response>
        /// /// <response code="400">Returned if the model couldn't be parsed or the survey couldn't be updated</response>
        [SwaggerResponse(200, type: typeof(ServiceResponse<SurveyResponseModel>), description: "Success.")]
        [SwaggerResponse(400, type: typeof(ServiceResponse<SurveyResponseModel>), description: "Error")]
        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] SurveyUpdateModel surveyModel)
        {
            try
            {
                var mappingRequest = this.mapper.Map<SurveyUpdateModel, SurveyUpdateDto>(surveyModel);
                var result = await this.surveyService.UpdateAsync(mappingRequest).ConfigureAwait(false);

                if (!result.IsSuccessful)
                {
                    return this.NotFound(result);
                }

                var mappingResult = this.mapper.Map<SurveyResponseDto, SurveyResponseModel>(result.Data);

                return this.Ok(mappingResult);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Action to delete a survey.
        /// </summary>
        /// <param name="id">Survey id.</param>
        /// <returns>Returns the created survey</returns>
        /// /// <response code="200">Returned if the survey was updated</response>
        /// /// <response code="400">Returned if the model couldn't be parsed or the survey couldn't be updated</response>
        [SwaggerResponse(200, type: typeof(ServiceResponse<bool>), description: "Success.")]
        [SwaggerResponse(400, type: typeof(ServiceResponse<bool>), description: "Error")]
        [SwaggerResponse(404, type: typeof(ServiceResponse<bool>), description: "Not found")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var result = await this.surveyService.DeleteAsync(id).ConfigureAwait(false);

                if (!result.IsSuccessful)
                {
                    return this.NotFound(result);
                }

                return this.Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Action to get survey results.
        /// </summary>
        /// <returns>Returns survey results</returns>
        /// <response code="200">Returns survey results from the database</response>
        /// <response code="400">Returned if the survey result couldn't be loaded</response>
        [SwaggerResponse(200, type: typeof(ServiceResponse<List<SurveyResultResponseModel>>), description: "Success.")]
        [SwaggerResponse(400, type: typeof(ServiceResponse<List<SurveyResultResponseModel>>), description: "Error")]
        [SwaggerResponse(404, type: typeof(ServiceResponse<List<SurveyResultResponseModel>>), description: "Not found")]
        [HttpGet("results")]
        public async Task<IActionResult> GetResults()
        {
            try
            {
                var result = await this.surveyService.GetSurveyResultsAsync().ConfigureAwait(false);

                if (!result.IsSuccessful)
                {
                    return this.NotFound(result);
                }

                var mappingResult = this.mapper.Map<List<SurveyResultResponseDto>, List<SurveyResultResponseModel>>(result.Data);

                return this.Ok(mappingResult);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Action to create survey result.
        /// </summary>
        /// <param name="surveyModel">Model to create survey result</param>
        /// <returns>Returns the created survey</returns>
        /// /// <response code="200">Returned if the survey was created</response>
        /// /// <response code="400">Returned if the model couldn't be parsed or the survey couldn't be saved</response>
        [SwaggerResponse(200, type: typeof(ServiceResponse<bool>), description: "Success.")]
        [SwaggerResponse(400, type: typeof(ServiceResponse<bool>), description: "Error")]
        [SwaggerResponse(404, type: typeof(ServiceResponse<bool>), description: "Not found")]
        [HttpPost("result")]
        public async Task<IActionResult> PostResult([FromBody] SurveyResultPostModel resultModel)
        {
            try
            {
                var mappingRequest = this.mapper.Map<SurveyResultPostModel, SurveyResultPostDto>(resultModel);

                mappingRequest.IpAddress = Request?.HttpContext?.Connection?.RemoteIpAddress?.ToString();

                var response = await this.surveyService.SurveyResultPostAsync(mappingRequest).ConfigureAwait(false);

                if (!response.IsSuccessful)
                {
                    return this.NotFound(response);
                }

                return this.Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
