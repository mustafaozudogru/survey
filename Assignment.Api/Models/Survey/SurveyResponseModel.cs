﻿namespace Assignment.Api.Models
{
    public class SurveyResponseModel
    {
        /// <summary>
        /// SurveyJson.
        /// </summary>
        public string SurveyJson { get; set; }
    }
}
