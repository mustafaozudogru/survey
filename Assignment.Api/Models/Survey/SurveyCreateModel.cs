﻿using System.ComponentModel.DataAnnotations;

namespace Assignment.Api.Models
{
    public class SurveyCreateModel
    {
        /// <summary>
        /// Name.
        /// </summary>
        [Required(ErrorMessage = "Please do not leave blank.")]
        [StringLength(100, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Name { get; set; }

        /// <summary>
        /// SurveyJson.
        /// </summary>
        [Required(ErrorMessage = "Please do not leave blank.")]
        public string SurveyJson { get; set; }
    }
}
