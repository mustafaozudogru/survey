﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Assignment.Api.Models
{
    public class SurveyUpdateModel
    {
        /// <summary>
        /// Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        [Required(ErrorMessage = "Please do not leave blank.")]
        [StringLength(20, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string Name { get; set; }

        /// <summary>
        /// SurveyJson.
        /// </summary>
        [Required(ErrorMessage = "Please do not leave blank.")]
        [StringLength(20, ErrorMessage = "The {0} value cannot exceed {1} characters. ")]
        public string SurveyJson { get; set; }
    }
}
