﻿using Assignment.Domain.src;
using Assignment.Persistence.src.Context.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Api.Models
{
    public class DataGenerator
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MainDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<MainDbContext>>()))
            {
                // Look for any board games already in database.
                if (context.Survey.Any())
                {
                    return;   // Database has been seeded
                }

                string readContents;
                using (StreamReader streamReader = new StreamReader("survey.json", Encoding.UTF8))
                {
                    readContents = streamReader.ReadToEnd();
                }

                context.Survey.Add(
                    new Survey()
                    {
                        Id = Guid.Parse("C2352892-6344-4B49-8EEE-D8A22C42F115"),
                        Name = "Survey",
                        SurveyJson = readContents,
                    });

                context.SaveChanges();
            }
        }
    }
}
