﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Assignment.Api.Models
{
    public class SurveyResultPostModel
    {
        /// <summary>
        /// ResultJson.
        /// </summary>
        [Required(ErrorMessage = "Please do not leave blank.")]
        public string ResultJson { get; set; }
    }
}
