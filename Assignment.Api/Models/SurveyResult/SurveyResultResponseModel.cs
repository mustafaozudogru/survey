﻿namespace Assignment.Api.Models
{
    /// <summary>
    /// Used to keep survey update model props.
    /// </summary>
    public class SurveyResultResponseModel
    {
        /// <summary>
        /// IpAddress.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// SurveyJson.
        /// </summary>
        public string ResultJson { get; set; }
    }
}
