﻿using Assignment.Api.Models;
using Assignment.Application.Abstraction.src.Survey.Contracts;
using AutoMapper;

namespace Assignment.Api.Mappings
{
    public class SurveyProfile : Profile
    {
        public SurveyProfile()
        {
            CreateMap<SurveyCreateModel, SurveyCreateDto>().ReverseMap();

            CreateMap<SurveyUpdateModel, SurveyUpdateDto>();

            CreateMap<SurveyResponseDto, SurveyResponseModel>();

            CreateMap<SurveyResultPostModel, SurveyResultPostDto>();

            CreateMap<SurveyResultResponseDto, SurveyResultResponseModel>();
        }
    }
}
