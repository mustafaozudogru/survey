﻿using Assignment.Application.Abstraction.src.Survey;
using Assignment.Application.src.SurveyServices;
using Assignment.Common.src.Helpers;
using Assignment.Persistence.src.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;

namespace Assignment.Infastructure.src.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterExternalServices(this IServiceCollection services)
        {
            services.AddTransient<ISurveyService, SurveyService>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IServiceResponseHelper, ServiceResponseHelper>();
        }
    }
}
