﻿using Assignment.Api.Controllers;
using Assignment.Api.Models;
using Assignment.Application.Abstraction.src.Survey;
using Assignment.Application.Abstraction.src.Survey.Contracts;
using Assignment.Common.src.Constants;
using Assignment.Common.src.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Assignment.Test.Controllers
{
    public class SurveyControllerTest
    {
        private readonly Mock<IMapper> mapperMock;
        private readonly SurveyController surveyController;
        private readonly Mock<ISurveyService> surveyService;
        private readonly SurveyCreateModel surveyCreateModel;
        private readonly SurveyUpdateModel surveyUpdateModel;
        private readonly SurveyCreateDto surveyCreateDto;
        private readonly SurveyUpdateDto surveyUpdateDto;
        private readonly SurveyResponseDto surveyResponseDto;
        private readonly SurveyResultResponseDto surveyResultResponseDto;
        private readonly SurveyResultPostDto surveyResultPostDto;
        private readonly SurveyResultPostModel surveyResultPostModel;

        private readonly Guid _id = Guid.Parse("5224ed94-6d9c-42ec-ba93-dfb11fe68931");

        public SurveyControllerTest()
        {
            mapperMock = new Mock<IMapper>();
            surveyService = new Mock<ISurveyService>();
            surveyController = new SurveyController(mapperMock.Object, surveyService.Object);

            surveyCreateModel = new SurveyCreateModel() { Name = "Survey", SurveyJson = "Survey Content" };
            surveyUpdateModel = new SurveyUpdateModel() { Id = _id, Name = "Survey-1", SurveyJson = "Survey Content-1" };

            surveyCreateDto = new SurveyCreateDto() { Name = "Survey", SurveyJson = "Survey Content" };
            surveyUpdateDto = new SurveyUpdateDto() { Id = _id, Name = "Survey-1", SurveyJson = "Survey Content-1" };
            surveyResponseDto = new SurveyResponseDto() { Id = _id, Name = "Survey", SurveyJson = "Survey Content" };
            surveyResultResponseDto = new SurveyResultResponseDto() { IpAddress = "111:222:3:4", ResultJson = "Survey Result Content" };
            surveyResultPostDto = new SurveyResultPostDto() { IpAddress = "111:222:3:4", ResultJson = "Survey Result Content" };
            surveyResultPostModel = new SurveyResultPostModel() { ResultJson = "Survey Result Content" };
        }

        [Fact]
        [Trait("GetById", "SurveyController")]
        public async Task GetById_ShouldReturnOkResult()
        {
            ServiceResponse<SurveyResponseDto> serviceResponse = new ServiceResponse<SurveyResponseDto>() { Data = surveyResponseDto, IsSuccessful = true, Message = CommonMessageConstants.Success, MessageCode = StatusCodes.Status200OK };

            this.surveyService.Setup(x => x.GetByIdAsync(_id)).ReturnsAsync(serviceResponse);

            var result = await surveyController.GetById(_id);

            Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("GetById", "SurveyController")]
        public async Task GetById_ReturnsNotFound()
        {
            ServiceResponse<SurveyResponseDto> serviceResponse = new ServiceResponse<SurveyResponseDto>() { Data = null, Message = CommonMessageConstants.NotFound, IsSuccessful = false, MessageCode = StatusCodes.Status404NotFound };

            this.surveyService.Setup(x => x.GetByIdAsync(_id)).ReturnsAsync(serviceResponse);

            var result = await surveyController.GetById(_id);

            Assert.IsAssignableFrom<NotFoundObjectResult>(result);
        }

        [Fact]
        [Trait("Get", "SurveyController")]
        public async Task GetAll_ShouldReturnOkResult()
        {
            ServiceResponse<List<SurveyResponseDto>> serviceResponse = new ServiceResponse<List<SurveyResponseDto>>() { Data = new List<SurveyResponseDto> { surveyResponseDto }, IsSuccessful = true, Message = CommonMessageConstants.Success, MessageCode = StatusCodes.Status200OK };

            this.surveyService.Setup(x => x.GetAsync()).ReturnsAsync(serviceResponse);

            var result = await surveyController.Get();

            Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Get", "SurveyController")]
        public async Task GetByName_ShouldReturnOkResult()
        {
            ServiceResponse<SurveyResponseDto> serviceResponse = new ServiceResponse<SurveyResponseDto>() { Data = surveyResponseDto, IsSuccessful = true, Message = CommonMessageConstants.Success, MessageCode = StatusCodes.Status200OK };

            this.surveyService.Setup(x => x.GetByIdAsync(_id)).ReturnsAsync(serviceResponse);

            var result = await surveyController.GetById(_id);

            Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Post", "SurveyController")]
        public async Task Post_ShouldReturnOkResult()
        {
            ServiceResponse<SurveyCreateDto> serviceResponse = new ServiceResponse<SurveyCreateDto>() { Data = surveyCreateDto, IsSuccessful = true, Message = CommonMessageConstants.Success, MessageCode = StatusCodes.Status200OK };

            this.mapperMock.Setup(s => s.Map<SurveyCreateModel, SurveyCreateDto>(It.IsAny<SurveyCreateModel>())).Returns(surveyCreateDto);

            this.surveyService.Setup(x => x.CreateAsync(It.IsAny<SurveyCreateDto>())).ReturnsAsync(serviceResponse);

            var result = await surveyController.Post(surveyCreateModel);

            Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Update", "SurveyController")]
        public async Task Update_ShouldReturnOkResult()
        {
            ServiceResponse<SurveyResponseDto> serviceResponse = new ServiceResponse<SurveyResponseDto>() { Data = surveyResponseDto, IsSuccessful = true, Message = CommonMessageConstants.Success, MessageCode = StatusCodes.Status200OK };

            this.mapperMock.Setup(s => s.Map<SurveyUpdateModel, SurveyUpdateDto>(It.IsAny<SurveyUpdateModel>())).Returns(surveyUpdateDto);

            this.surveyService.Setup(x => x.UpdateAsync(It.IsAny<SurveyUpdateDto>())).ReturnsAsync(serviceResponse);

            var result = await surveyController.Update(surveyUpdateModel);

            Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Update", "SurveyController")]
        public async Task Update_ReturnsNotFound()
        {
            ServiceResponse<SurveyResponseDto> serviceResponse = new ServiceResponse<SurveyResponseDto>() { Data = null, Message = CommonMessageConstants.NotFound, IsSuccessful = false, MessageCode = StatusCodes.Status404NotFound };

            this.mapperMock.Setup(s => s.Map<SurveyUpdateModel, SurveyUpdateDto>(It.IsAny<SurveyUpdateModel>())).Returns(surveyUpdateDto);

            this.surveyService.Setup(x => x.UpdateAsync(It.IsAny<SurveyUpdateDto>())).ReturnsAsync(serviceResponse);

            var result = await surveyController.Update(surveyUpdateModel);


            Assert.IsAssignableFrom<NotFoundObjectResult>(result);
        }

        [Fact]
        [Trait("Delete", "SurveyController")]
        public async Task Delete_ShouldReturnOkResult()
        {
            ServiceResponse<bool> serviceResponse = new ServiceResponse<bool>() { Data = true, IsSuccessful = true, Message = CommonMessageConstants.Success, MessageCode = StatusCodes.Status200OK };

            this.surveyService.Setup(x => x.DeleteAsync(_id)).ReturnsAsync(serviceResponse);

            var result = await surveyController.Delete(_id);

            Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Delete", "SurveyController")]
        public async Task Delete_ReturnsNotFound()
        {
            ServiceResponse<bool> serviceResponse = new ServiceResponse<bool>() { Data = false, Message = CommonMessageConstants.NotFound, IsSuccessful = false, MessageCode = StatusCodes.Status404NotFound };

            this.surveyService.Setup(x => x.DeleteAsync(_id)).ReturnsAsync(serviceResponse);

            var result = await surveyController.Delete(_id);

            Assert.IsAssignableFrom<NotFoundObjectResult>(result);
        }

        [Fact]
        [Trait("Get", "SurveyController")]
        public async Task GetResults_ShouldReturnOkResult()
        {
            ServiceResponse<List<SurveyResultResponseDto>> serviceResponse = new ServiceResponse<List<SurveyResultResponseDto>>() { Data = new List<SurveyResultResponseDto> { surveyResultResponseDto }, IsSuccessful = true, Message = CommonMessageConstants.Success, MessageCode = StatusCodes.Status200OK };

            this.surveyService.Setup(x => x.GetSurveyResultsAsync()).ReturnsAsync(serviceResponse);

            var result = await surveyController.GetResults();

            Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Post", "SurveyController")]
        public async Task PostResult_ShouldReturnOkResult()
        {
            ServiceResponse<bool> serviceResponse = new ServiceResponse<bool>() { Data = true, IsSuccessful = true, Message = CommonMessageConstants.Success, MessageCode = StatusCodes.Status200OK };

            this.mapperMock.Setup(s => s.Map<SurveyResultPostModel, SurveyResultPostDto>(It.IsAny<SurveyResultPostModel>())).Returns(surveyResultPostDto);

            this.surveyService.Setup(x => x.SurveyResultPostAsync(It.IsAny<SurveyResultPostDto>())).ReturnsAsync(serviceResponse);

            var connectionMock = new Mock<ConnectionInfo>(MockBehavior.Strict);
            connectionMock.SetupGet(c => c.RemoteIpAddress).Returns(new IPAddress(16885952));

            var httpContext = new Mock<HttpContext>(MockBehavior.Strict);
            httpContext.SetupGet(x => x.Connection).Returns(connectionMock.Object);

            var result = await surveyController.PostResult(surveyResultPostModel);

            Assert.IsAssignableFrom<OkObjectResult>(result);
        }
    }
}
