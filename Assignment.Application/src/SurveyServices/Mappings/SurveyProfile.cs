﻿using Assignment.Application.Abstraction.src.Survey.Contracts;
using Assignment.Domain.src;
using AutoMapper;

namespace Assignment.Application.src.SurveyServices.Mappings
{
    public class SurveyProfile : Profile
    {
        public SurveyProfile()
        {
            CreateMap<SurveyCreateDto, Survey>().ReverseMap();

            CreateMap<SurveyUpdateDto, Survey>();

            CreateMap<Survey, SurveyResponseDto>();

            CreateMap<SurveyResultPostDto, SurveyResult>();

            CreateMap<SurveyResult, SurveyResultResponseDto>();            
        }
    }
}
