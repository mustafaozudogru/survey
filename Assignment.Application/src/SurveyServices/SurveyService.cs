﻿using Assignment.Application.Abstraction.src.Survey;
using Assignment.Application.Abstraction.src.Survey.Contracts;
using Assignment.Common.src.Constants;
using Assignment.Common.src.Helpers;
using Assignment.Common.src.Models;
using Assignment.Domain.src;
using Assignment.Persistence.src.UnitOfWork;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.Application.src.SurveyServices
{
    public class SurveyService : ISurveyService
    {
        private readonly IMapper mapper;
        private readonly IServiceResponseHelper serviceResponseHelper;
        private readonly IUnitOfWork unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="SurveyService"/> class.
        /// </summary>
        /// <param name="mapper">IMapper.</param>
        /// <param name="serviceResponseHelper">IServiceResponseHelper.</param>
        /// <param name="unitOfWork">IUnitOfWork.</param>
        public SurveyService(IMapper mapper, IServiceResponseHelper serviceResponseHelper, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.serviceResponseHelper = serviceResponseHelper;
        }

        /// <inheritdoc>/>
        public async Task<ServiceResponse<SurveyCreateDto>> CreateAsync(SurveyCreateDto request)
        {
            var mappingResult = this.mapper.Map<Survey>(request);

            await this.unitOfWork.Survey.AddAsync(mappingResult).ConfigureAwait(false);
            await this.unitOfWork.CommitAsync();

            return this.serviceResponseHelper.SetSuccess(request);
        }

        /// <inheritdoc>/>
        public async Task<ServiceResponse<List<SurveyResponseDto>>> GetAsync()
        {
            var result = await this.unitOfWork.Survey.GetAllAsync().ConfigureAwait(false);

            if (result == null || !result.Any())
            {
                return this.serviceResponseHelper.SetError<List<SurveyResponseDto>>(null, CommonMessageConstants.NotFound, StatusCodes.Status404NotFound);
            }

            return this.serviceResponseHelper.SetSuccess(this.mapper.Map<List<SurveyResponseDto>>(result));
        }

        /// <inheritdoc>/>
        public async Task<ServiceResponse<SurveyResponseDto>> GetByIdAsync(Guid id)
        {
            var result = await this.unitOfWork.Survey.GetByIdAsync(id);

            if (result == null)
            {
                return this.serviceResponseHelper.SetError<SurveyResponseDto>(null, CommonMessageConstants.NotFound, StatusCodes.Status404NotFound);
            }

            return this.serviceResponseHelper.SetSuccess(this.mapper.Map<SurveyResponseDto>(result));
        }

        /// <inheritdoc>/>
        public async Task<ServiceResponse<List<SurveyResponseDto>>> GetByNameByWithIpControlAsync(string name, string ipAddress)
        {
            var surveyResultResponse = await this.unitOfWork.SurveyResult.Find(m => m.IpAddress.Contains(ipAddress)).ConfigureAwait(false);

            if (surveyResultResponse != null && surveyResultResponse.Any())
            {
                return this.serviceResponseHelper.SetError<List<SurveyResponseDto>>(null, CommonMessageConstants.AlreadyAdded, StatusCodes.Status500InternalServerError);
            }

            var result = await this.unitOfWork.Survey.Find(m => m.Name.Contains(name)).ConfigureAwait(false);

            if (result == null || !result.Any())
            {
                return this.serviceResponseHelper.SetError<List<SurveyResponseDto>>(null, CommonMessageConstants.NotFound, StatusCodes.Status404NotFound);
            }

            return this.serviceResponseHelper.SetSuccess(this.mapper.Map<List<SurveyResponseDto>>(result.ToList()));
        }

        /// <inheritdoc>/>
        public async Task<ServiceResponse<SurveyResponseDto>> UpdateAsync(SurveyUpdateDto request)
        {
            var result = await this.unitOfWork.Survey.GetByIdAsync(request.Id).ConfigureAwait(false);

            if (result == null)
            {
                return this.serviceResponseHelper.SetError<SurveyResponseDto>(null, CommonMessageConstants.NotFound, StatusCodes.Status404NotFound);
            }

            result.Name = request.Name;
            result.SurveyJson = request.SurveyJson;

            await this.unitOfWork.CommitAsync();

            return this.serviceResponseHelper.SetSuccess(this.mapper.Map<SurveyResponseDto>(result));
        }

        /// <inheritdoc>/>
        public async Task<ServiceResponse<bool>> DeleteAsync(Guid id)
        {
            var result = await this.unitOfWork.Survey.GetByIdAsync(id).ConfigureAwait(false);

            if (result == null)
            {
                return this.serviceResponseHelper.SetError(false, CommonMessageConstants.NotFound, StatusCodes.Status404NotFound);
            }

            this.unitOfWork.Survey.Remove(result);
            await this.unitOfWork.CommitAsync();

            return this.serviceResponseHelper.SetSuccess(true);
        }

        /// <inheritdoc>/>
        public async Task<ServiceResponse<bool>> SurveyResultPostAsync(SurveyResultPostDto request)
        {
            var result = await this.unitOfWork.SurveyResult.Find(m => m.IpAddress == request.IpAddress).ConfigureAwait(false);

            if (result != null && result.Any())
            {
                return this.serviceResponseHelper.SetError<bool>(false, CommonMessageConstants.AlreadyAdded, StatusCodes.Status302Found);
            }

            var mappingResult = this.mapper.Map<SurveyResult>(request);

            await this.unitOfWork.SurveyResult.AddAsync(mappingResult).ConfigureAwait(false);
            await this.unitOfWork.CommitAsync();

            return this.serviceResponseHelper.SetSuccess(true);
        }

        /// <inheritdoc>/>
        public async Task<ServiceResponse<List<SurveyResultResponseDto>>> GetSurveyResultsAsync()
        {
            var result = await this.unitOfWork.SurveyResult.GetAllAsync().ConfigureAwait(false);

            if (result == null || !result.Any())
            {
                return this.serviceResponseHelper.SetError<List<SurveyResultResponseDto>>(null, CommonMessageConstants.NotFound, StatusCodes.Status404NotFound);
            }

            return this.serviceResponseHelper.SetSuccess(this.mapper.Map<List<SurveyResultResponseDto>>(result));
        }
    }
}
