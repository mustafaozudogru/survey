﻿using Assignment.Persistence.src.Repositories.SurveyRepository;
using Assignment.Persistence.src.Repositories.SurveyResultRepository;
using System;
using System.Threading.Tasks;

namespace Assignment.Persistence.src.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ISurveyRepository Survey { get; }

        ISurveyResultRepository SurveyResult { get; }

        Task<int> CommitAsync();
    }
}
