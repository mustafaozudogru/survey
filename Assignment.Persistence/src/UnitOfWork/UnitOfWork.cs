﻿using Assignment.Persistence.src.Context.Main;
using Assignment.Persistence.src.Repositories.SurveyRepository;
using Assignment.Persistence.src.Repositories.SurveyResultRepository;
using System.Threading.Tasks;

namespace Assignment.Persistence.src.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MainDbContext mainDbContext;
        private SurveyRepository surveyRepository;
        private SurveyResultRepository surveyResultRepository;

        public UnitOfWork(MainDbContext mainDbContext)
        {
            this.mainDbContext = mainDbContext;
        }

        public ISurveyRepository Survey => this.surveyRepository = this.surveyRepository ?? new SurveyRepository(this.mainDbContext);

        public ISurveyResultRepository SurveyResult => this.surveyResultRepository = this.surveyResultRepository ?? new SurveyResultRepository(this.mainDbContext);

        public async Task<int> CommitAsync()
        {
            return await this.mainDbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            this.mainDbContext.Dispose();
        }
    }
}
