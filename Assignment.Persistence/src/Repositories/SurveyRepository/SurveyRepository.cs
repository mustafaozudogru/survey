﻿using Assignment.Domain.src;
using Assignment.Persistence.src.Context.Main;

namespace Assignment.Persistence.src.Repositories.SurveyRepository
{
    public class SurveyRepository : Repository<Survey>, ISurveyRepository
    {
        public SurveyRepository(MainDbContext context)
            : base(context)
        {
        }
    }
}
