﻿using Assignment.Domain.src;

namespace Assignment.Persistence.src.Repositories.SurveyRepository
{
    public interface ISurveyRepository : IRepository<Survey>
    {
    }
}
