﻿using Assignment.Domain.src;
using Assignment.Persistence.src.Context.Main;

namespace Assignment.Persistence.src.Repositories.SurveyResultRepository
{
    public class SurveyResultRepository : Repository<SurveyResult>, ISurveyResultRepository
    {
        public SurveyResultRepository(MainDbContext context)
            : base(context)
        {
        }
    }
}
