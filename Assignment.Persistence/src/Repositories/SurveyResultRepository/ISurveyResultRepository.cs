﻿using Assignment.Domain.src;

namespace Assignment.Persistence.src.Repositories.SurveyResultRepository
{
    public interface ISurveyResultRepository : IRepository<SurveyResult>
    {
    }
}
