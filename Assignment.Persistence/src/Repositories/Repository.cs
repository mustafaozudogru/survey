﻿using Assignment.Persistence.src.Context.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Assignment.Persistence.src.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly MainDbContext mainDbContext;

        public Repository(MainDbContext mainDbContext)
        {
            this.mainDbContext = mainDbContext;
        }

        public async Task AddAsync(TEntity entity)
        {
            await mainDbContext.Set<TEntity>().AddAsync(entity);
        }

        public void UpdateAsync(TEntity entity)
        {
            mainDbContext.Entry(entity).State = EntityState.Modified;
        }

        public async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            await mainDbContext.Set<TEntity>().AddRangeAsync(entities);
        }

        public async Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return await mainDbContext.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await mainDbContext.Set<TEntity>().ToListAsync();
        }

        public ValueTask<TEntity> GetByIdAsync(Guid id)
        {
            return mainDbContext.Set<TEntity>().FindAsync(id);
        }

        public void Remove(TEntity entity)
        {
            mainDbContext.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            mainDbContext.Set<TEntity>().RemoveRange(entities);
        }

        public Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return mainDbContext.Set<TEntity>().SingleOrDefaultAsync(predicate);
        }
    }
}
