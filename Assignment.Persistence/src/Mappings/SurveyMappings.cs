﻿using Assignment.Domain.src;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.IO;
using System.Text;

namespace Assignment.Persistence.src.Mappings
{
    public static class SurveyMappings
    {
        public static void OnModelCreating(EntityTypeBuilder<Survey> builder)
        {

            builder.Property(e => e.Id).HasDefaultValueSql("(newid())");

            builder.Property(e => e.Name).IsRequired().HasMaxLength(100);

            builder.Property(e => e.SurveyJson).IsRequired();

            //SeedData(builder);
        }

        private static void SeedData(EntityTypeBuilder<Survey> builder)
        {
            string readContents;
            using (StreamReader streamReader = new StreamReader("survey.json", Encoding.UTF8))
            {
                readContents = streamReader.ReadToEnd();
            }

            var survey = new Survey()
            {
                Id = Guid.Parse("C2352892-6344-4B49-8EEE-D8A22C42F115"),
                Name = "Survey",
                SurveyJson = readContents,
            };

            builder.HasData(survey);
        }
    }
}
