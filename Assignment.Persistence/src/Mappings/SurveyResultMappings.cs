﻿using Assignment.Domain.src;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Assignment.Persistence.src.Mappings
{
    public static class SurveyResultMappings
    {
        public static void OnModelCreating(EntityTypeBuilder<SurveyResult> builder)
        {

            builder.Property(e => e.Id).HasDefaultValueSql("(newid())");

            builder.Property(e => e.IpAddress).IsRequired().HasMaxLength(100);

            builder.Property(e => e.ResultJson).IsRequired();
        }
    }
}
