﻿using Assignment.Domain.src;
using Assignment.Persistence.src.Mappings;
using Microsoft.EntityFrameworkCore;

namespace Assignment.Persistence.src.Context.Main
{
    public class MainDbContext : DbContext
    {
        public DbSet<Survey> Survey { get; set; }

        public DbSet<SurveyResult> SurveyResult { get; set; }

        public MainDbContext(DbContextOptions<MainDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Survey>(SurveyMappings.OnModelCreating);

            builder.Entity<SurveyResult>(SurveyResultMappings.OnModelCreating);
        }
    }
}
