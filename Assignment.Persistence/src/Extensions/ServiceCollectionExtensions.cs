﻿using Assignment.Persistence.src.Context.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Assignment.Persistence.src.Extensions
{
    /// <summary>
    /// ServiceCollectionExtensions.
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds the in memory database.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        /// <param name="configuration">The configuration.</param>
        public static void AddInMemoryInfrastructure(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddDbContext<MainDbContext>((options) =>
            {
                options.UseInMemoryDatabase(databaseName: "DbSurvey");
            });
        }

        /// <summary>
        /// Adds the persistence infrastructure.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        /// <param name="configuration">The configuration.</param>
        public static void AddPersistenceInfrastructure(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            var connectionString = configuration["Data:MainDbContext:ConnectionString"];

            serviceCollection.AddDbContext<MainDbContext>((options) =>
            {
                options.ConfigureDatabase(connectionString, configuration["Data:MainDbContext:MigrationsAssembly"]);
            });
        }

        /// <summary>
        /// Configures the database.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="migrationAssembly">The migration assembly.</param>
        public static void ConfigureDatabase(this DbContextOptionsBuilder builder, string connectionString, string migrationAssembly)
        {
            builder.UseSqlServer(connectionString, b => b.MigrationsAssembly(migrationAssembly));
        }
    }
}
