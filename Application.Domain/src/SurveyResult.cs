﻿using System;

namespace Assignment.Domain.src
{
    public class SurveyResult
    {
        public Guid Id { get; set; }

        /// <summary>
        /// IpAddress.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// ResultJson.
        /// </summary>
        public string ResultJson { get; set; }
    }
}
