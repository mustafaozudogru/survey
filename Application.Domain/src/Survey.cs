﻿using System;

namespace Assignment.Domain.src
{
    public class Survey
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// SurveyJson.
        /// </summary>
        public string SurveyJson { get; set; }
    }
}
