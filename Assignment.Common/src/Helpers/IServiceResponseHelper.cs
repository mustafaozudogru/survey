﻿using Assignment.Common.src.Models;

namespace Assignment.Common.src.Helpers
{
    public interface IServiceResponseHelper
    {
        /// <summary>
        /// Used to create generic error model.
        /// </summary>
        ServiceResponse<T> SetError<T>(T data, string errorMessage, int statusCode = 500);

        /// <summary>
        /// Used to create generic success model.
        /// </summary>
        ServiceResponse<T> SetSuccess<T>(T data);
    }
}
