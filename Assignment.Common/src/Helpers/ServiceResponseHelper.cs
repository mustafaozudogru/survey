﻿using Assignment.Common.src.Constants;
using Assignment.Common.src.Models;
using Microsoft.AspNetCore.Http;

namespace Assignment.Common.src.Helpers
{
    public class ServiceResponseHelper : IServiceResponseHelper
    {
        /// <summary>
        /// Used to create generic error model.
        /// </summary>
        ServiceResponse<T> IServiceResponseHelper.SetError<T>(T data, string errorMessage, int statusCode)
        {
            return new ServiceResponse<T>() { Data = data, Message = errorMessage, IsSuccessful = false, MessageCode = statusCode };
        }

        /// <summary>
        /// Used to create generic success model.
        /// </summary>
        ServiceResponse<T> IServiceResponseHelper.SetSuccess<T>(T data)
        {
            return new ServiceResponse<T>() { Data = data, Message = CommonMessageConstants.Success, IsSuccessful = true, MessageCode = StatusCodes.Status200OK };
        }
    }
}
