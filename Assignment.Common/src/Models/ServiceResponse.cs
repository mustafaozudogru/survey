﻿namespace Assignment.Common.src.Models
{
    /// <summary>
    /// generic response model.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ServiceResponse<T>
    {
        /// <summary>
        /// response message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// response message code.
        /// </summary>
        public int MessageCode { get; set; }

        /// <summary>
        /// IsSuccessful.
        /// </summary>
        public bool IsSuccessful { get; set; }

        /// <summary>
        /// response data.
        /// </summary>
        public T Data { get; set; }
    }
}
