﻿namespace Assignment.Common.src.Constants
{
    public static class CommonMessageConstants
    {
        /// <summary>
        /// Ok.
        /// </summary>
        public const string Ok = "Ok";

        /// <summary>
        /// Success.
        /// </summary>
        public const string Success = "Success";

        /// <summary>
        /// AlreadyAdded.
        /// </summary>
        public const string AlreadyAdded = "Already added";

        /// <summary>
        /// Not found.
        /// </summary>
        public const string NotFound = "No records found";

        /// <summary>
        /// MappingError.
        /// </summary>
        public const string MappingError = "Mapping error.";
    }
}
